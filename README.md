## Collection of instructions and material for FV heavy Higgs signal generation 

#### Relevant links 

Model registration https://its.cern.ch/jira/browse/AGENE-1857 

MC samples request https://its.cern.ch/jira/browse/ATLMCPROD-8639 

Talk in HBSM https://indico.cern.ch/event/931827/ 

Validation plots http://tvazquez.web.cern.ch/tvazquez/Validation_2HDMplots/ 

Twiki with the model https://twiki.org/cgi-bin/view/Sandbox/FlavorChangingNeutralHiggs 

#### Set-up and run

Currently simple set-up, chain:  
    
    source setup_release.sh
    python test_all_jos.py
    
Can also run on the grid by typing 
    
    source setup_release.sh
    lsetup panda
    python  test_all_jos_grid.py

#### Available processes 

Charge conjugate processes split only when applicable to intial state (different pdfs x if valence or sea quarks, different kinematics). 
Charge conjugate decays are always added up. 

#### TRUTH Derivations 
(please see here for available derivation https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TruthDAOD)

Set-up

    setupATLAS
    asetup 21.2.6.0,AthDerivation,here 

(work for all MC15 at least)

Run 
    
    Reco_tf.py --inputEVNTFile EVNT.root --outputDAODFile test.pool.root --reductionConf TRUTH0
    
#### Analyse the derivations

Example of code here https://gitlab.cern.ch/htx/MCValidationTools. The code needs some updates but should still work on lxplus, I hope. 

As mentioned in the readme of the code, need to fix the configuation before running https://gitlab.cern.ch/htx/MCValidationTools/blob/master/MyTruthAnalysis/Root/MyxAODTruthAnalysis.cxx#L35. PROCESS can be GENERIC.  
    
    
#### Relevant parameters value

Default values  

    rho_tt = 1
    rho_tc = 1
    tho_tu = 1
    kAZH = 1
    m_s0 = 400 GeV
    m_a0 = 400 GeV

Actual values used in the generation are setup via JO name extension (currently possible to setup the first four couplings and the S0 mass). 
Make sure you have a combination of couplings which doesn't dump the amplitute to zero. 

Reference paper https://arxiv.org/pdf/1710.07260.pdf 

In general you can read the coupling values from here MG5_aMC_v2_3_3/models/gen2HDM_UFO/parameters.py     

#### Samples commands

Save the JO you would like to run in 100xxx/10000 folder, then type  

    Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=500 --outputEVNTFile=evgen.root
    
to run locally or 

    pathena --trf "Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=500 --outputEVNTFile %OUT.EVNT.root" --outDS user.orlando.gen_test_fv_1 --extFile YourAwesomeJO.py

to run on the grid 

#### Versions of the various models from /afs/cern.ch/work/o/orlando/public 

* gen2HDM_UFO_original: original UFO model from the authors. 
* gen2HDM_UFO_hack_1: hacked version to fix crash with showering in Pythia8 when generating S0.
* gen2HDM_UFO: current version, fix crash with Pythia8 and includes A0->ZH decays
