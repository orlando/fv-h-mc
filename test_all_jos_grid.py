import os
import sys

user='orlando'
prod_tag='v10.8'

#list_of_jos=['mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_AZH1_tZH.py','mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_tt1_AZH1_ttZH.py']
list_of_jos=['mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_AZH1_tZH.py','mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_tt1_AZH1_ttZH.py']
#list_of_jos=['mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_tt1_AZH1_ttZH.py']

list_of_mass_points=['300','400','500','600']
#list_of_mass_points=['600']

for mass in list_of_mass_points:
    for job_option in list_of_jos: 
        print('Processing mass: '+mass)
        job_option = job_option.replace("500", mass)
        # For labeling the log.generate output and job command 
        process=job_option.split('_')[-1].split('.')[0]
        print('Processing: '+process)
        rm_command='rm 100xxx/100000/*.*'
        cp_command='cp '+mass+'/'+job_option+' 100xxx/100000/'
        command_to_run='pathena --trf "Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=10000 --outputEVNTFile %OUT.EVNT.root" --outDS user.'+user+'.gen_test_fvh_'+prod_tag+'_'+process+'_'+mass+' --extFile '+job_option
        os.system(rm_command)
        os.system(cp_command)
        os.system(command_to_run)
        


        


