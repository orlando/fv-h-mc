import os
import sys

list_of_jos=['mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_AZH1_tZH.py','mc.PhPy8EG_NNPDF31_MA500_tu1_tc1_tt1_AZH1_ttZH.py','mc.PhPy8EG_NNPDF31_MS500_tc1_tu1_sstt.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_tbartq.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_tt1_tbartt.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_tt1_ttt.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_tt1_tttq.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_tt1_tttt.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_ttq.py','mc.PhPy8EG_NNPDF31_MS500_tu1_tc1_tt1_tA.py']

list_of_mass_points=['300','400','500','600']

for mass in list_of_mass_points:
    for job_option in list_of_jos: 
        print('Processing mass: '+mass)
        job_option = job_option.replace("500", mass)
        # For labeling the log.generate output and job command 
        process=job_option.split('_')[-1].split('.')[0]
        print('Processing: '+process)
        cp_command='cp '+mass+'/'+job_option+' 100xxx/100000/'
        command_to_run='Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=500 --outputEVNTFile=evgen_'+process+'.root'
        log_generate_rename='mv log.generate log.generate.'+process+'_'+mass
        clean_folder='source ${PWD}/clean_folder.sh'
        rm_command='rm 100xxx/100000/*.*'
        os.system(cp_command)
        os.system(command_to_run)
        os.system(log_generate_rename)
        os.system(clean_folder)
        os.system(rm_command)

