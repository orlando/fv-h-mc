import os
import sys

base_name='user.orlando.gen_test_fvh_v2.0'

list_of_ds=['sstt_300','sstt_400','sstt_500','sstt_600','tA_300','tA_400','tA_500','tA_600','tbartq_300','tbartq_400','tbartq_500','tbartq_600','tbartt_300','tbartt_400','tbartt_500','tbartt_600','ttq_300','ttq_400','ttq_500','ttq_600','ttt_300','ttt_400','ttt_500','ttt_600','tttq_300','tttq_400','tttq_500','tttq_600','tttt_400','tttt_500','tttt_600','ttZH_300','ttZH_400','ttZH_500','ttZH_600','tZH_300','tZH_400','tZH_500','tZH_600']


for element in list_of_ds:
    folder=base_name+'_'+element+'.log'
    cd_command='/tmp/orlando/'+folder
    uncompress_command='tar -xvf user.orlando*'
    move_command="cp tarball*/log.generate ../log.generate_"+element
    os.chdir(cd_command)
    os.system(uncompress_command)
    os.system(move_command)
    os.chdir('/tmp/orlando/')
    
    
